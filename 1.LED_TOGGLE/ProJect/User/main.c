/**
 * @file 			main.c
 * @author		I-Power
 * @version		V1.0.0
 * @date			12/12/2020
 * @link 			chrome-extension://oemmndcbldboiebfnladdacbdfmadadm/https://stm32-base.org/assets/pdf/boards/original-schematic-STM32F103C8T6-STM32_Smart_V2.0.pdf
 * @mail			intelligentpower20@gmail.com
 */

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdint.h>

#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"



//================ Funtion =====================//
void GPIO_init(void);
void delay_1s(uint32_t s);
void delay_ms(uint32_t ms);
void delay_us(uint32_t us);
//=============================================//
#define TOGGLE1

int main()
{ 
  SysTick_Config(64000000/1000); 
	GPIO_init();

	while(1)
	{
#ifdef TOGGLE1
		    GPIO_WriteBit(GPIOC, GPIO_Pin_13, Bit_RESET);
				delay_ms(100);
				GPIO_WriteBit(GPIOC, GPIO_Pin_13, Bit_SET);
				delay_ms(100);
#endif
#ifdef TOGGLE2
				GPIO_SetBits(GPIOC, GPIO_Pin_13);
				delay_ms(500);
				GPIO_ResetBits(GPIOC,GPIO_Pin_13);
				delay_ms(500);
#endif

	}
}



//===============================================//
void GPIO_init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

/**************************** LED **********************************/
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13; 
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	
}

//---------------DELAY----------------------//

void delay_1s(uint32_t s) // delay 1 giay
{ 
	int i,j;
  for (j=0;j<s;j++)
   {
    for(i=0;i<8000000;i++);
   }
}

void delay_ms(uint32_t ms) // delay 1 miligiay
{ 
	int i,j;
	for(j=0;j<ms;j++)
	 {
	  for(i=0;i<8000;i++);
	 }
}
void delay_us(uint32_t us) //delay 1 micro_giay//
{
	int i,j;
	for(j=0;j<us;j++)
	{
		for(i=0;i<8;i++);
	}
}

//===============================================//




#ifdef  USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
 { 
 /* User can add his own implementation to report the file name and line number,
 ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

 /* Infinite loop */
 while (1)
 {
 }
}
#endif

